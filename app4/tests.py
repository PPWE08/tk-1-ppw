from django.test import TestCase
from django.urls import reverse
from .models import Relawan
from .forms import relawanForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
import requests
import json

# Create your tests here.
class UnitTest(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get(reverse('app4:relawan'))
        self.assertEqual(response.status_code, 200)

    def test_template_yang_digunakan_dari_halaman_relawan(self):
        response = self.client.get(reverse('app4:relawan'))
        self.assertTemplateUsed(response, 'app4.html')
    
    def test_models_dari_halaman_hotline(self):
        Relawan.objects.create(
            nama='abc' , 
            nama_panggilan='a',
            no_hp='0812121212',
            email='abc@gmail.com',
            identitas='KTP',
            no_identitas='123123123123',
            alamat='asadadasasadasdasda',
            jenis_kelamin='Perempuan',
            kota_kelahiran='london',
            tanggal_lahir='2001-12-12',
            agama='Islam',
            status='Belum Menikah',
            pekerjaan='ingfluenza',
            peminatan='Relawan Medis',
            saya_menyetujui_syarat_dan_ketentuan_yang_berlaku=True,
            )
        n_relawan = Relawan.objects.all().count()
        self.assertEqual(n_relawan,1)

    def test_register(self):
        user = User.objects.create_user('tes', 'test@testing.com', 'passwordtest')
        user.save()
        self.assertEqual(User.objects.all().count(), 1)

    def test_login_user_success(self):
        user = User.objects.create_user('tes123', 'test@testing.com', 'passwordtest')
        user.save()
        logged_in = self.client.login(username='tes123', password='passwordtest')
        self.assertTrue(logged_in)

    def test_login_user_gagal(self):
        user = User.objects.create_user('tes123', 'test@testing.com', 'passwordtest')
        user.save()
        logged_in = self.client.login(username='tes123', password='salah')
        self.assertFalse(logged_in)

    def test_user_not_logged_in(self):
        response = self.client.get('/relawan/')
        isi_html = response.content.decode('utf8')
        self.assertIn('<h1 style="margin-top: 50px; margin-bottom: 50px; margin-left: auto; margin-right: auto;">Silahkan Login Terlebih Dahulu</h1>', isi_html)
        self.assertIn('<a href="/otentikasi/login/"><button class="btn btn-primary">Login Disini</button></a> ', isi_html)



    '''
    def test_form_valid_dari_halaman_relawan(self):
        data = {
            'nama':'abc' , 
            'nama_panggilan':'a',
            'no_hp':'0812121212',
            'email':'abc@gmail.com',
            'identitas':'1',
            'no_identitas':'123123123123',
            'alamat':'asadadasasadasdasda',
            'jenis_kelamin':'2',
            'kota_kelahiran':'london',
            'tanggal_lahir':'12-12-2001',
            'agama':'1',
            'status':'1',
            'pekerjaan':'ingfluenza',
            'peminatan':'1',
            'saya_menyetujui_syarat_dan_ketentuan_yang_berlaku':True,
            }
        form = relawanForm(data=data)
        self.assertTrue(form.is_valid())
        
        self.assertEqual(form.cleaned_data['nama'],"abc")
        self.assertEqual(form.cleaned_data['nama_panggilan'],"a")
        self.assertEqual(form.cleaned_data['no_hp'],"0812121212")
        self.assertEqual(form.cleaned_data['email'],"abc@gmail.com")
        self.assertEqual(form.cleaned_data['identitas'],'KTP')
        self.assertEqual(form.cleaned_data['no_identitas'],"123123123123")
        self.assertEqual(form.cleaned_data['alamat'],"asadadasasadasdasda")
        self.assertEqual(form.cleaned_data['jenis_kelamin'],'Perempuan')
        self.assertEqual(form.cleaned_data['kota_kelahiran'],"london")
        self.assertEqual(form.cleaned_data['tanggal_lahir'],"2001-12-12")
        self.assertEqual(form.cleaned_data['agama'],'Islam')
        self.assertEqual(form.cleaned_data['status'],'Belum Kawin')
        self.assertEqual(form.cleaned_data['pekerjaan'],"ingfluenza")
        self.assertEqual(form.cleaned_data['peminatan'],'Relawan Medis')
        self.assertEqual(form.cleaned_data['saya_menyetujui_syarat_dan_ketentuan_yang_berlaku'],True)
    
    def test_post_form_dari_halaman_relawan(self):
        test_nama = 'abc'
        test_nama_panggilan = 'a'
        test_no_hp = "0812121212"
        test_email = "abc@gmail.com"
        test_identitas = "1"
        test_no_identitas = "123123123123"
        test_alamat = "asadadasasadasdasda"
        test_jenis_kelamin = "2" 
        test_kota_kelahiran = "london"
        test_tanggal_lahir = '2001-12-12'
        test_agama ="1"
        test_status = "1"
        test_pekerjaan = "ingfluenza"
        test_peminatan = "1"
        test_saya_menyetujui_syarat_dan_ketentuan_yang_berlaku = True
        response = self.client.post('/relawan' , {
            'nama' : test_nama, 
            'nama_panggilan' : test_nama_panggilan,
            'no_hp': test_no_hp,
            'email':test_email,
            'identitas':test_identitas,
            'no_identitas':test_no_identitas,
            'alamat':test_alamat,
            'jenis_kelamin':test_jenis_kelamin,
            'kota_kelahiran':test_kota_kelahiran,
            'tanggal_lahir':test_tanggal_lahir,
            'agama':test_agama,
            'status':test_status,
            'pekerjaan':test_pekerjaan,
            'peminatan':test_peminatan,
            'saya_menyetujui_syarat_dan_ketentuan_yang_berlaku':test_saya_menyetujui_syarat_dan_ketentuan_yang_berlaku
            })
        self.assertEqual(response.status_code,200)
        
        form = relawanForm(data={
            'nama' : test_nama,
            'nama_panggilan' : test_nama_panggilan,
            'no_hp': test_no_hp,
            'email':test_email,
            'identitas':test_identitas,
            'no_identitas':test_no_identitas,
            'alamat':test_alamat,
            'jenis_kelamin':test_jenis_kelamin,
            'kota_kelahiran':test_kota_kelahiran,
            'tanggal_lahir':test_tanggal_lahir,
            'agama':test_agama,
            'status':test_status,
            'pekerjaan':test_pekerjaan,
            'peminatan':test_peminatan,
            'saya_menyetujui_syarat_dan_ketentuan_yang_berlaku':test_saya_menyetujui_syarat_dan_ketentuan_yang_berlaku
            })
        self.assertTrue(form.is_valid())
        
        self.assertEqual(form.cleaned_data['nama'],"abc")
        self.assertEqual(form.cleaned_data['nama_panggilan'],"a")
        self.assertEqual(form.cleaned_data['no_hp'],"0812121212")
        self.assertEqual(form.cleaned_data['email'],"abc@gmail.com")
        self.assertEqual(form.cleaned_data['identitas'],"KTP")
        self.assertEqual(form.cleaned_data['no_identitas'],"123123123123")
        self.assertEqual(form.cleaned_data['alamat'],"asadadasasadasdasda")
        self.assertEqual(form.cleaned_data['jenis_kelamin'],"Perempuan")
        self.assertEqual(form.cleaned_data['kota_kelahiran'],"london")
        self.assertEqual(form.cleaned_data['tanggal_lahir'],"12-12-2001")
        self.assertEqual(form.cleaned_data['agama'],"Islam")
        self.assertEqual(form.cleaned_data['status'],"Belum Menikah")
        self.assertEqual(form.cleaned_data['pekerjaan'],"ingfluenza")
        self.assertEqual(form.cleaned_data['peminatan'],"Relawan Medis")
        self.assertEqual(form.cleaned_data['saya_menyetujui_syarat_dan_ketentuan_yang_berlaku'],True)
    '''
        