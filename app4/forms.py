from django import forms

class relawanForm(forms.Form):
    pilihan_identitas = (
        ('KTP','KTP'),
        ('SIM','SIM'),        
    )
    pilihan_jk = (
        ('Laki-Laki','Laki-Laki'),
        ('Perempuan','Perempuan'),
    )
    pilihan_agama = (
        ('Islam','Islam'),
        ('Kristen Protestan','Kristen Protestan'),
        ('Katolik','Katolik'),
        ('Hindu','Hindu'),
        ('Buddha','Buddha'),
        ('Konghucu','Konghucu'),
        ('Memilih untuk tidak mengisi','Memilih untuk tidak mengisi')
    )
    pilihan_status = (
        ('Belum Kawin','Belum Kawin'),
        ('Sudah Kawin','Sudah Kawin'),
    )
    pilihan_cluster = (
        ('Relawan Medis','Relawan Medis'),
        ('Relawan Jurnalis','Relawan Jurnalis'),
        ('Relawan Media Sosial','Relawan Media Sosial'),
        ('Relawan Creative','Relawan Creative'),
        ('Relawan Fundraising','Relawan Fundraising'),
        ('Relawan Bencana','Relawan Bencana'),
        ('Relawan Pendidikan','Relawan Pendidikan'),
        ('Relawan Programmer','Relawan Programmer'),
    )
    nama = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Isi dengan nama lengkap anda',
        'type' : 'text',
        'required': True,
    }))

    nama_panggilan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Isi dengan nama panggilan anda',
        'type' : 'text',
        'required': True,
    }))

    no_hp = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Cth: 081234567890',
        'required': True,
    }))
    email = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Cth: nama@gmail.com',
        'type' : 'text',
        'required': True,
    }))

    identitas = forms.ChoiceField(choices=pilihan_identitas)
    no_identitas = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Cth: 1234567890XXXX',
        'type' : 'text',
        'required': True,
    }))

    alamat = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Jl. Nama jalan, Nomor rumah, RT/RW, Kelurahan, Kecamatan, Kode Pos, Kota/Kabupaten, Provinsi',
        'type' : 'text',
        'required': True,
    }))
    
    jenis_kelamin = forms.ChoiceField(choices=pilihan_jk)

    kota_kelahiran = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Cth: Depok',
        'type' : 'text',
        'required': True,
    }))

    tanggal_lahir = forms.DateField(widget=forms.DateInput(attrs={
        'class': 'form-control',
        'placeholder' : 'dd/mm/yyyy',
        'type' : 'date',
        'required': True,
    }))

    agama = forms.ChoiceField(choices=pilihan_agama)
    status = forms.ChoiceField(choices=pilihan_status)
    pekerjaan = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Cth: Pegawai Negeri',
        'type' : 'text',
        'required': True,
    }))

    peminatan = forms.ChoiceField(choices=pilihan_cluster)
    saya_menyetujui_syarat_dan_ketentuan_yang_berlaku = forms.BooleanField(required=True)
