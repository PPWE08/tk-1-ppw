from django.urls import path

from . import views

app_name = 'app4'

urlpatterns = [
    path('', views.relawan, name='relawan'),
    path('success/', views.success, name='success'),
    path('confirm/', views.confirm, name='confirm')
]
