from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

class Relawan(models.Model):
    pilihan_identitas = (
        ('KTP','KTP'),
        ('SIM','SIM'),        
    )
    pilihan_jk = (
        ('Laki-Laki','Laki-Laki'),
        ('Perempuan','Perempuan'),
    )
    pilihan_agama = (
        ('Islam','Islam'),
        ('Kristen Protestan','Kristen Protestan'),
        ('Katolik','Katolik'),
        ('Hindu','Hindu'),
        ('Buddha','Buddha'),
        ('Konghucu','Konghucu'),
        ('Memilih untuk tidak mengisi','Memilih untuk tidak mengisi')
    )
    pilihan_status = (
        ('Belum Kawin','Belum Kawin'),
        ('Sudah Kawin','Sudah Kawin'),
    )
    pilihan_cluster = (
        ('Relawan Medis','Relawan Medis'),
        ('Relawan Jurnalis','Relawan Jurnalis'),
        ('Relawan Media Sosial','Relawan Media Sosial'),
        ('Relawan Creative','Relawan Creative'),
        ('Relawan Fundraising','Relawan Fundraising'),
        ('Relawan Bencana','Relawan Bencana'),
        ('Relawan Pendidikan','Relawan Pendidikan'),
        ('Relawan Programmer','Relawan Programmer'),
    )
    nama = models.CharField(max_length=50,default='')
    nama_panggilan = models.CharField(max_length=50,default='')
    no_hp = models.CharField(max_length=200, default='')
    email = models.CharField(max_length=200,default='')
    identitas = models.CharField(max_length=200, choices=pilihan_identitas)
    no_identitas = models.CharField(max_length=20, default='')
    alamat = models.CharField(max_length=500, default='')
    jenis_kelamin = models.CharField(max_length=100, choices=pilihan_jk)
    kota_kelahiran = models.CharField(max_length=200,default='')
    tanggal_lahir = models.DateField()
    agama = models.CharField(max_length=200, choices=pilihan_agama)
    status = models.CharField(max_length=200, choices=pilihan_status)
    pekerjaan = models.CharField(max_length=200, default='')
    peminatan = models.CharField(max_length=200, choices=pilihan_cluster)
    saya_menyetujui_syarat_dan_ketentuan_yang_berlaku = models.BooleanField(default=False)