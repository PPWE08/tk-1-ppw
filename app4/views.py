from django.shortcuts import render,redirect
from .forms import relawanForm
from .models import Relawan
from django.core.mail import send_mail
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse

formrelawan = ''
form = ''
def relawan(request):
    if request.method == "POST":
        global form
        form = relawanForm(request.POST)
        if form.is_valid():            
            global formrelawan
            formrelawan = Relawan()
            formrelawan.nama = form.cleaned_data['nama']
            formrelawan.nama_panggilan = form.cleaned_data['nama_panggilan']
            formrelawan.no_hp = form.cleaned_data['no_hp']
            formrelawan.email = form.cleaned_data['email']
            formrelawan.identitas = form.cleaned_data['identitas']
            formrelawan.no_identitas = form.cleaned_data['no_identitas']
            formrelawan.alamat = form.cleaned_data['alamat']
            formrelawan.jenis_kelamin = form.cleaned_data['jenis_kelamin']
            formrelawan.kota_kelahiran = form.cleaned_data['kota_kelahiran']
            formrelawan.tanggal_lahir = form.cleaned_data['tanggal_lahir']
            formrelawan.agama = form.cleaned_data['agama']
            formrelawan.status = form.cleaned_data['status']
            formrelawan.pekerjaan = form.cleaned_data['pekerjaan']
            formrelawan.peminatan = form.cleaned_data['peminatan']  
            
            context = {
                "nama" : form.cleaned_data['nama'],
                "nama_panggilan" : form.cleaned_data['nama_panggilan'],
                "no_hp" : form.cleaned_data['no_hp'],
                "email" : form.cleaned_data['email'],
                "identitas" : form.cleaned_data['identitas'],
                "no_identitas" : form.cleaned_data['no_identitas'],
                "alamat" : form.cleaned_data['alamat'],
                "jenis_kelamin" : form.cleaned_data['jenis_kelamin'],
                "kota_kelahiran" : form.cleaned_data['kota_kelahiran'],
                "tanggal_lahir" : form.cleaned_data['tanggal_lahir'],
                "agama" : form.cleaned_data['agama'],
                "status" : form.cleaned_data['status'],
                "pekerjaan" : form.cleaned_data['pekerjaan'],
                "peminatan" : form.cleaned_data['peminatan']
            }
            
        return JsonResponse(context, safe=False)
    else:
        #relawanall = Relawan.objects.all()
        form = relawanForm()
    
    context = {'form' : form}
    return render(request,'app4.html',context)

def success(request):
    return render(request, 'success.html')

def confirm(request):
    global form
    #form.save()
    global formrelawan
    formrelawan.save()
    send_mail('Selamat Datang Relawan Baru!',
            'Halo! Terimakasih telah melakukan registrasi menjadi relawan kami.\nKami akan segera menghubungi anda bila ada pekerjaan.\nSalam Kebaikan :)\n-Kelompok 8-',
            'ppwkelompok8@gmail.com',
            [formrelawan.email],
            fail_silently=False)  
    return HttpResponseRedirect('/relawan/success')