from django.test import TestCase, Client, LiveServerTestCase
from .models import Isi
from .views import FormPelanggan, KonfirmasiPembayaran, KonfirmasiTerakhir
from django.shortcuts import render, redirect, reverse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login

# Create your tests here.
class Test(TestCase):
    def setUp(self):
        self.Url_kp = reverse("app2:kp")

    def template_home_ada(self):
        response = Client().get('app2/')
        self.assertEquals(response.status_code, 200)
    
    def template_home_bener(self):
        response = Client.get('/app2')
        self.assertTemplateUsed(response, "FormPelanggan.html")

    def test_buat_form(self):
        isi = Isi(
            nama_orang = "Bambang",
            nomor_hp = "08090348",
            alamat_email = "Bambang@gmail.com",
            jumlah_donasi = "10000000",
            nama_bank = "Bank BCA"
        )
        isi.save()
        self.assertEquals(Isi.objects.all().count(), 1)
    
    def test_belum_login(self):
        response = Client().get('/donasiuang/')
        isi = response.content.decode('utf8')
        self.assertIn('<h2 style="margin-top: 50px; margin-bottom: 50px; margin-left: auto; margin-right: auto;">Silahkan login terlebih dahulu</h2>', isi)
        self.assertIn('<a href="/otentikasi/login/"><button class="btn btn-primary">Login Disini</button></a>', isi)
    
    def test_login(self):
        user = User.objects.create(username='iniUser')
        user.set_password('user123')
        user.save()
        status_login = Client().login(username='iniUser', password='user123')
        self.assertTrue(status_login)