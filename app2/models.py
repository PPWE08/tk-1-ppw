from django.db import models

# Create your models here.
BANK = (
    ('', 'Pilih Bank'),
    ('Bank Mandiri', 'Bank Mandiri'),
    ('Bank BCA', 'Bank BCA'),
    ('Bank BNI', 'Bank BNI'),
    ('Bank BRI', 'Bank BRI'),
    ('Bank Niaga', 'Bank Niaga')
)

class Isi(models.Model):
    nama_orang = models.CharField(max_length = 100)
    nomor_hp = models.CharField(max_length = 100)
    alamat_email = models.CharField(max_length = 100)
    nama_bank = models.CharField(max_length = 100, choices=BANK)
    jumlah_donasi = models.CharField(max_length = 100)
