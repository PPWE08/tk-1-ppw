from django import forms
from .models import Isi
from django.forms import ModelForm

class FormUtama(forms.ModelForm):
   

    class Meta:
        model = Isi
        nama_orang = forms.CharField(widget = forms.TextInput(attrs={
            "placeholder" : "Nama"
        }), label = "")

        nomor_hp = forms.CharField(widget = forms.TextInput(attrs={
            "placeholder" : "Nomor HP"
        }))

        jumlah_donasi = forms.CharField(widget = forms.TextInput(attrs={
            "placeholder" : "Jumlah Donasi"
        }))

        alamat_email = forms.EmailField()
        fields = "__all__"