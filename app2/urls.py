from django.urls import path
from . import views

app_name = 'app2'

urlpatterns = [
    path('', views.FormPelanggan, name='fp'),
    path('KonfirmasiPembayaran/', views.KonfirmasiPembayaran, name='kp'),
    path('KonfirmasiTerakhir/', views.KonfirmasiTerakhir, name='kt'),
]