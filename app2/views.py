from django.shortcuts import render, redirect, reverse
from .models import Isi
from .forms import FormUtama
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
import json
formu = ""
# Create your views here.
def FormPelanggan(request):
    if (request.method == "POST"):
        global formu
        formu = FormUtama(request.POST)
        if formu.is_valid():
            context = {
            "nama_orang" : formu.cleaned_data['nama_orang'],
            "nomor_hp" : formu.cleaned_data['nomor_hp'],
            "alamat_email" : formu.cleaned_data['alamat_email'],
            "nama_bank" : formu.cleaned_data['nama_bank'],
            "jumlah_donasi" : formu.cleaned_data['jumlah_donasi'],
            }
            #formu.save()
            #return render(request, 'KonfirmasiPembayaran.html', context)
            return JsonResponse(context, safe=False)
    else:
        formu = FormUtama()
    
    context = {
        "formu" : formu,
        #"is_tambah_form" : True
    }
    return render(request, 'FormPelanggan.html', context)

def KonfirmasiPembayaran(request):
    #if request.method == "POST":
        #formu = FormUtama(request.POST)
        #if formu.is_valid():
            #formu.save()
    global formu
    formu.save()
    return redirect(reverse("app2:kt"))
        
def KonfirmasiTerakhir(request):
    return render(request, 'KonfirmasiTerakhir.html')