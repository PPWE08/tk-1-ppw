from django.urls import path

from . import views

app_name = 'otentikasi'

urlpatterns = [
    path('signup/', views.signup, name='signup'),
    path('login/', views.loginacc, name='loginacc'),
    path('logout/', views.logoutacc, name='logoutacc'),
]
