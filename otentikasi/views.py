from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect

# Create your views here.
def loginacc(request):

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)

            return redirect('/')
        
        else:
            messages.info(request, 'Username atau Password salah')
        
        return redirect('/otentikasi/login/')

    return render(request, 'login.html')

def logoutacc(request):
    logout(request)

    return redirect('/')

def signup(request):
    if(request.method == "POST"):
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        user = User.objects.create_user(username, email, password)
        user.save()
        return HttpResponseRedirect('/otentikasi/login/')
    else:
        return render(request, 'signup.html')
