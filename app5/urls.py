from django.urls import path

from .views import listrelawan
from .views import detailrelawan
from .views import deleterelawan

app_name = 'app5'

urlpatterns = [
    path('', listrelawan, name = 'listrelawan'),
    path('detailrelawan/<str:pk>', detailrelawan, name = 'readRelawan'),
    path('deleterelawan/<str:pk>/', deleterelawan, name = 'deleteRelawan')
]