from django import forms
from .models import Barang
from django.forms import ModelForm
from tempus_dominus.widgets import DatePicker

class FormBarang(ModelForm):
    class Meta:
        model = Barang
        widgets = {
            'nama_donatur': forms.TextInput(attrs={'placeholder': 'Nama'}),
            'no_handphone': forms.TextInput(attrs={'placeholder': 'No handphone'}),
            'date' : forms.DateTimeInput(attrs={
                'class': 'form-control',
                'placeholder' : 'dd/mm/yyyy',
                'type' : 'date',
                }),
            #'date' : DatePicker(),
            'place' : forms.TextInput(attrs={'placeholder': 'Alamat'}),

        }
        labels = {
            'date': 'Waktu penjemputan barang',
            'place': 'Alamat penjemputan barang',
        }
        # widgets = {
        #     'nama_donatur': forms.HiddenInput(),
        #     'no_handphone': forms.HiddenInput(),
        #     'date' : forms.HiddenInput(),
                
        #     # }, format='%d-%m-%Y %H:%M'
        #     #'date': forms.TextInput(attrs={'placeholder': 'Tanggal'}),
            
        #     'place' : forms.HiddenInput(),

        # }
        
        fields = '__all__'