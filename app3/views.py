from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .forms import FormBarang
from django.http import JsonResponse
import json
form=""
# Create your views here.
def form_barang(request):
    if(request.method == "POST"):
        global form
        form = FormBarang(request.POST)
        #formjson = FormBarang(request.POST).toJSON()
        if form.is_valid():
            context = {
            "nama_donatur" : form.cleaned_data['nama_donatur'],
            "no_handphone" : form.cleaned_data['no_handphone'],
            "date" : form.cleaned_data['date'],
            "barang" : form.cleaned_data['barang'],
            "place" : form.cleaned_data['place'],
            }
            #form.save()
            #return render(request, "app3/konfirmasi.html", context)
            return JsonResponse(context, safe=False)
            
            #return HttpResponseRedirect('/donasibarang/success')
    else:
        form = FormBarang()
       
    context = {'form' : form}
    return render(request, "app3/donasibarang.html", context)

def success(request):
    return render(request, 'app3/sukses.html')

def confirm(request):
    #if(request.method == "POST"):
        #form=FormBarang(request.POST)
    global form
    form.save()
    return HttpResponseRedirect('/donasibarang/success')