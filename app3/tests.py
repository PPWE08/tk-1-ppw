from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from .views import form_barang, confirm
from .models import Barang
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login

class Test(TestCase):
    def test_index_url_is_exist(self):
        response = Client().get('/donasibarang/')
        self.assertEqual(response.status_code, 200)

    def test_index_index_func(self):
        found = resolve('/donasibarang/')
        self.assertEqual(found.func, form_barang)

    def test_index_using_template(self):
        response = Client().get('/donasibarang/')
        self.assertTemplateUsed(response, 'app3/donasibarang.html')

    def test_Event_model_create_new_object(self):
        barang = Barang(
            nama_donatur ="aziz",
            no_handphone="123",
            barang="Pakaian",
            date="2020-11-29",
            place="Jakarta"
            )
        barang.save()
        self.assertEqual(Barang.objects.all().count(), 1)

    def test_success_url_is_exist(self):
        response = Client().get('/donasibarang/success')
        self.assertEqual(response.status_code, 200)

    def test_user_not_logged_in(self):
        response = Client().get('/donasibarang/')
        isi_html_kembalian = response.content.decode('utf8')
        self.assertIn('<h2 align="center" style="margin-top: 20px;">Anda belum login, harap login terlebih dahulu</h2>', isi_html_kembalian)
        self.assertIn('<a href="/otentikasi/login/"><button class="btn btn-primary">Login Disini</button></a>', isi_html_kembalian)

    def test_create_user(self):
        user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        user.save()
        self.assertEqual(User.objects.all().count(), 1)

    def test_login_user_success(self):
        user = User.objects.create(username='testuser')
        user.set_password('testpassword')
        user.save()
        logged_in = Client().login(username='testuser', password='testpassword')
        self.assertTrue(logged_in)

    def test_login_user_fail(self):
        user = User.objects.create(username='testuser')
        user.set_password('testpassword')
        user.save()
        logged_in = Client().login(username='testuser', password='wrongpassword')
        self.assertFalse(logged_in)

    # def test_user_logged_in(self):
    #     user = User.objects.create(username='testuser')
    #     user.set_password('testpassword')
    #     user.save()
    #     logged_in = Client().login(username='testuser', password='testpassword')
    #     response = Client().get('/donasibarang/')
    #     isi_html_kembalian = response.content.decode('utf8')
    #     self.assertIn('<h2 align="center" style="margin-top: 20px;">Isi Form Donasi</h2>', isi_html_kembalian)
    #     self.assertIn('<form action="{% url \'app3:form_barang\' %}" method="post">', isi_html_kembalian)

# @tag('functional')
# class FunctionalTestCase(LiveServerTestCase):
#     """Base class for functional test cases with selenium."""

#     @classmethod
#     def setUpClass(cls):
#         super().setUpClass()
#         # Change to another webdriver if desired (and update CI accordingly).
#         options = webdriver.chrome.options.Options()
#         # These options are needed for CI with Chromium.
#         options.headless = True  # Disable GUI.
#         options.add_argument('--no-sandbox')
#         options.add_argument('--disable-dev-shm-usage')
#         cls.selenium = webdriver.Chrome(options=options)

#     @classmethod
#     def tearDownClass(cls):
#         cls.selenium.quit()
#         super().tearDownClass()


# class MainTestCase(TestCase):
#     def test_root_url_status_200(self):
#         response = self.client.get('/')
#         self.assertEqual(response.status_code, 200)
#         # You can also use path names instead of explicit paths.
#         response = self.client.get(reverse('main:home'))
#         self.assertEqual(response.status_code, 200)


# class MainFunctionalTestCase(FunctionalTestCase):
#     def test_root_url_exists(self):
#         self.selenium.get(f'{self.live_server_url}/')
#         html = self.selenium.find_element_by_tag_name('html')
#         self.assertNotIn('not found', html.text.lower())
#         self.assertNotIn('error', html.text.lower())
